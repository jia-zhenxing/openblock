
import { defineAsyncComponent, toRaw, ref } from 'vue';
OpenBlock.onInitedPromise().then(async () => {
    let Env = OpenBlock.Env;
    let settings = ref(OB_IDE.webConfig.defaultEditorSettings || {
        "hiddenEnv": ["editorExt"],
        enabledUI: {
            "dataset": true,
            "assets": true,
            "exportModule": true,
            "actionGroup": true,
            "functionGroup": true,
            "structGroup": true,
            "stateGroup": true
        }
    });
    async function loadConfig(p) {
        let _settings = await OpenBlock.VFS.partition.config.get('editor.json');
        if (_settings && _settings != settings) {
            settings.value = _settings;
        }
    }
    OpenBlock.VFS.partition.config.on('put', loadConfig);
    await loadConfig();
    let component = defineAsyncComponent(async () => {
        let [templateResp] = await Promise.all([
            axios({
                url: 'js/htmls/editorSettings/editorSettings.html',
                responseType: 'text'
            }),
        ]);
        return {
            name: 'editorSettings',
            template: templateResp.data,
            data() {
                return {
                    enabled: true, settings,
                    itemStyle: {
                        width: '180px'
                    },
                    groupStyle: {
                        "display": "flex",
                        "flex-direction": "row",
                        "flex-wrap": "wrap",
                        "padding-left": "15px"
                    }
                }
            },
            computed: {
                env() {
                    let env = [...Env.instances.values()];
                    return env;
                }
            },
            watch: {
                enabled(v1, v2) {
                    if (!v1) {
                        OB_IDE.editorSettings.save();
                        OB_IDE.removeComponent(this);
                    }
                }
            },
            methods: {
            }
        }
    });
    OB_IDE.editorSettings = {
        settings,
        setUIEnabled(name, enabled) {
            settings.value.enabledUI[name] = !!enabled;
            OB_IDE.editorSettings.save();
        },
        toggleUIEnabled(name) {
            settings.value.enabledUI[name] = settings.value.enabledUI[name] === false;
            OB_IDE.editorSettings.save();
        },
        checkUIEnabled(name) {
            if (settings.value.enabledUI[name] === false) {
                return false;
            } else {
                return true;
            }
        },
        save() {
            OpenBlock.VFS.partition.config.put('editor.json', settings.value);
        }
    };
    OB_IDE.addSiderBottomBotton({
        icon: 'md-settings',
        name: 'editorSettings',
        async onClick() {
            OB_IDE.addComponent(component);
        }
    });
});